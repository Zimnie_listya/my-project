from Exit_task.flowers import Date
from Exit_task.flowers import HB
from Exit_task.flowers import Casual

flower_by_mnth = {
    Date: {'January': 'img/Гипсофил.png',
           'February': 'img/Подсолнухи.png'},
    HB: {'January': 'img/Орхидея.png',
         'February': 'img/Гортензия.png'},
    Casual: {'January': 'img/Альстромерия.png',
             'February': 'img/Гербера.png'}
}
