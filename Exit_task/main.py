import telebot
from telebot import types
from flowers_by_mnth import flowers_by_mnth
from Exit_task.flowers import *

global keyboard_one
global keyboard_two
answer = {}
choice = {}

token = '1878002515:AAGnaVKt8N1F7Onvy5LNFvi9mE_nUFFC-L4'
bot = telebot.TeleBot(token)


@bot.message_handler(commands=['info'])
def info(message):
    send_mess_info = 'Я подбираю цветы в подарок по поводу и без.' \
                     '\nВыберите повод, расскажите мне месяц рождения того, кому вы хоите подарить цветы.\n' \
                     '\n/start\n - запуск бота и отображение списка поводов' \
                     '\n/help\n - расскажет как запустить бота'

    bot.send_message(message.chat.id, send_mess_info, parse_mode='html')


@bot.message_handler(commands=['help'])
def help_com(message):
    send_mess_help = f"<b>Привет {message.from_user.first_name}</b>!\nНапиши команду /start для подбора цветка."
    bot.send_message(message.chat.id, send_mess_help, parse_mode='html')


@bot.message_handler(commands=['start'])
def start(message):
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton(text='Свидание', callback_data=f'keyboard1_answer_1'))
    markup.add(types.InlineKeyboardButton(text='День рождения', callback_data=f'keyboard1_answer_2'))
    markup.add(types.InlineKeyboardButton(text='Без повода', callback_data=f'keyboard1_answer_3'))

    send_mess_start = f"Привет, {message.from_user.first_name}. Я помогу подобрать тебе цветы в подарок. Предлагаю " \
                      f"сначала выбрать повод. "
    bot.send_message(message.chat.id, send_mess_start, reply_markup=markup)
    bot.register_next_step_handler(message, birth)


@bot.callback_query_handler(func=lambda call: 'keyboard_one' in call.data)
def query_handler1(call):
    global keyboard_one
    keyboard_one = call.data
    birth(call.message)


def birth(message):
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton(text='Январь', callback_data=f'keyboard2_answer_1'))
    markup.add(types.InlineKeyboardButton(text='Февраль', callback_data=f'keyboard2_answer_2'))
    markup.add(types.InlineKeyboardButton(text='Март', callback_data=f'keyboard2_answer_3'))
    markup.add(types.InlineKeyboardButton(text='Апрель', callback_data=f'keyboard2_answer_4'))
    markup.add(types.InlineKeyboardButton(text='Май', callback_data=f'keyboard2_answer_5'))
    markup.add(types.InlineKeyboardButton(text='Июнь', callback_data=f'keyboard2_answer_6'))
    markup.add(types.InlineKeyboardButton(text='Июль', callback_data=f'keyboard2_answer_7'))
    markup.add(types.InlineKeyboardButton(text='Август', callback_data=f'keyboard2_answer_8'))
    markup.add(types.InlineKeyboardButton(text='Сентябрь', callback_data=f'keyboard2_answer_9'))
    markup.add(types.InlineKeyboardButton(text='Октябрь', callback_data=f'keyboard2_answer_10'))
    markup.add(types.InlineKeyboardButton(text='Ноябрь', callback_data=f'keyboard2_answer_11'))
    markup.add(types.InlineKeyboardButton(text='Декабрь', callback_data=f'keyboard2_answer_12'))

    send_mess_birth = f"Теперь мне нужно узнать месяц рождения человека, которому подбираю цветок."
    bot.send_message(message.chat.id, send_mess_birth, reply_markup=markup)


@bot.callback_query_handler(func=lambda call: 'keyboard_two' in call.data)
def query_handler2(call):
    global keyboard_two
    keyboard_two = call.data
    # result(call.message, keyboard_one, call.data )
    result(call.message)


def result(chat_id):
        print("Ищу цветок", end="...")
        global choice

        chat_id = str(chat_id)

        if choice.get(chat_id, None) is None:
            choice[chat_id] = {}

        if result == Date.January:
            choice[chat_id]['Date.January'] = choice[chat_id].get('Date', 'January')


    # answer = ''
    # img = ''
    #
    # if keyboard_one == 'keyboard1_answer_1' and keyboard_two == 'keyboard2_answer_1':
    #     answer = {Date: Janyary}
    #     img = 'img/Гипсофил.png'
    # elif keyboard_one == 'keyboard1_answer_2' and keyboard_two == 'keyboard2_answer_1':
    #     answer = January_HB
    #     img = 'img/Орхидея.png'
    # elif keyboard_one == 'keyboard1_answer_3' and keyboard_two == 'keyboard2_answer_1':
    #     answer = January_casual
    #     img = 'img/Альстромерия.png'
    #
    # if img:
    #     img = open(img, 'rb')
    #     bot.send_photo(message.chat.id, img, answer)
    #     img.close()
    # else:
    #     bot.send_message(message.chat.id, answer)

global choice
choice = {}

#
# def create_user_pokemon(message, pokemon_type_id, pokemon_name):
#     print(f"Начало создания объекта Pokemon для chat id = {message.chat.id}")
#     global state
#     user_pokemon = Pokemon(name=pokemon_name,
#                            pokemon_type=PokemonType(pokemon_type_id))
#
#     if state.get(message.chat.id, None) is None:
#         state[message.chat.id] = {}
#     state[message.chat.id]['user_pokemon'] = user_pokemon
#
#     image_filename = pokemon_by_type[user_pokemon.pokemon_type][user_pokemon.name]
#     bot.send_message(message.chat.id, 'Твой выбор:')
#     with open(f"../images/{image_filename}", 'rb') as file:
#         bot.send_photo(message.chat.id, file, user_pokemon)
#
#     print(f"Завершено создание объекта Pokemon для chat id = {message.chat.id}")

#
# global state
# # обращение по ключу ко вложенному словарю для получения объекта покемона пользователя
# user_pokemon = state[message.chat.id]['user_pokemon']
#
# # обращение по ключу ко вложенному словарю для получения объекта покемона npc
# pokemon_npc = state[message.chat.id]['npc_pokemon']




# def birth_date(message):
#     global BIRTH
#     bot.send_message(message.chat.id, 'Теперь мне нужно узнать месяц рождения человека, которому подбираю цветок.')
#     BIRTH[message.from_user.id] = ""
#     if BIRTH > 12:
#         send_mess_birth_1 = f"Введите корректный месяц. От 01 до 12."
#         bot.send_message(message.chat.id, send_mess_birth_1)
#     elif BIRTH == 0:
#         send_mess_birth_2 = f"Введите корректный месяц. От 01 до 12."
#         bot.send_message(message.chat.id, send_mess_birth_2)
#     elif len(BIRTH) < 2:
#         send_mess_birth_3 = f"Введите месяц в двухзначном виде"
#     elif type(BIRTH) is not int:
#         send_mess_birth_3 = f"Месяц должен быть введен в цифровом формате."
#     bot.register_next_step_handler(message, query_handler)


# @bot.callback_query_handler(func=lambda call: 'keyboard1' in call.data)
# def query_handler(call):
#     bot.answer_callback_query(callback_query_id=call.id, text='Я нашел подходящий цветок!')
#     answer = ''
#     img = ''
#     if call.data == 'keyboard1_answer_1' and call.data == 'keyboard2_answer_1':
#         answer = January_date
#         img = 'img/Гипсофил.png'
#     elif call.data == 'keyboard1_answer_2' and call.data == 'keyboard2_answer_1':
#         answer = January_HB
#         img = 'img/Орхидея.png'
#     elif call.data == 'keyboard1_answer_3' and call.data == 'keyboard2_answer_1':
#         answer = January_casual
#         img = 'img/Альстромерия.png'


if __name__ == '__main__':
    print('Starting bot...')
    bot.polling(none_stop=True, interval=0)
