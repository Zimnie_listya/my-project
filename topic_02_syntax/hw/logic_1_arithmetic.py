"""
Функция arithmetic.

Принимает 3 аргумента: первые 2 - числа, третий - операция, которая должна быть произведена над ними.
Если третий аргумент +, сложить их;
если —, то вычесть;
если *, то умножить;
если /, то разделить (первое на второе).
В остальных случаях вернуть строку "Unknown operator".
Вернуть результат операции.
"""


def arithmetic(a, b, c):
    if c == '+':
        return a+b
    if c == '-':
        return a-b
    if c == '*':
        return a*b
    if c == "/":
        return a/b
    else:
        return "Unknown operator"


if __name__ == '__main__':
    result = arithmetic(2, 2, 1)
    print(result)
