"""
Функция if_3_do.

Принимает число.
Если оно больше 3, то увеличить число на 10, иначе уменьшить на 10.
Вернуть результат.
"""


def if_3_do(a):
    if a > 3:
        return a + 10
    else:
        return a - 10


if __name__ == '__main__':
    result = if_3_do(5)
    print(result)
