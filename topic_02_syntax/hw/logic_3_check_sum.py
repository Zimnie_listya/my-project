"""
Функция check_sum.

Принимает 3 числа.
Вернуть True, если можно взять какие-то два из них и в сумме получить третье, иначе False
"""
def check_sum(a, b, c):
    if a + b == c or a + c == b or c + b == a:
        return True
    else:
        return False


if __name__ == '__main__':
    result = check_sum(2, 0, 0)
    print(result)



