"""
Функция print_hi.

Принимает число n.
Выведите на экран n раз фразу "Hi, friend!"
Если число <= 0, тогда вывести пустую строку.
Пример: n=3, тогда в результате "Hi, friend!Hi, friend!Hi, friend!"
"""


def print_hi(n):
    if n <= 0:
        print('')
    else:
        print("Hi, friend!"*n)


if __name__ == '__main__':
    result = print_hi(3)
    print(result)