"""
Функция sum_1_112_3.

Вернуть сумму 1+4+7+10+...109+112.
"""

def sum_1_112_3():
     i = range(1, 113, 3)
     return sum (i)


if __name__ == '__main__':
    result = sum_1_112_3()
    print(result)

