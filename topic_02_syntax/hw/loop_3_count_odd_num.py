"""
Функция count_odd_num.

Принимает натуральное число (целое число > 0).
Верните количество нечетных цифр в этом числе.
Если число меньше или равно 0, то вернуть "Must be > 0!".
Если число не целое (не int, а другой тип данных), то вернуть "Must be int!".
"""


def count_odd_num(b):

    if type(b)!= int:
        return "Must be int!"
    elif b <= 0:
        return "Must be > 0!"

    odd = 0
    while 0 < b:
        if b % 2 == 1:
            odd += 1
        b //= 10

    return odd


if __name__ == '__main__':
    result = count_odd_num(135)
    print(result)
