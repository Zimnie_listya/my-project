"""
Функция print_symbols_if.

Принимает строку.

Если строка нулевой длины, то вывести строку "Empty string!".

Если длина строки больше 5, то вывести первые три символа и последние три символа.
Пример: string='123456789' => result='123789'

Иначе вывести первый символ столько раз, какова длина строки.
Пример: string='345' => result='333'
"""


def print_symbols_if(string):
    my_str_length = len(string)
    my_str = str(string)
    if my_str_length == 0:
        print('Empty string!')
    elif my_str_length > 5:
        print(my_str[0:3] + my_str[-3:])
    else:
        print(my_str[0] * my_str_length)


if __name__ == '__main__':
    print_symbols_if('123456789')