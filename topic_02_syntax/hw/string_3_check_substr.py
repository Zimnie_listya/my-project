"""
Функция check_substr.

Принимает две строки.
Если меньшая по длине строка содержится в большей, то возвращает True,
иначе False.
Если строки равны, то False.
Если одна из строк пустая, то True.
"""

def check_substr(str1, str2):
    if len(str1) < len(str2):
        return str1 in str2
    elif len(str1) > len(str2):
        return str2 in str1
    elif len(str1) == len(str2):
        return False
    elif len(str1) or len(str2) == "":
        return False

if __name__ == '__main__':
    check_substr('Hellooooow', 'Hot')