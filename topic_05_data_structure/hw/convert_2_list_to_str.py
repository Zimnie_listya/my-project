"""
Функция list_to_str.

Принимает 2 аргумента: список и разделитель (строка).

Возвращает (строку полученную разделением элементов списка разделителем,
количество разделителей в получившейся строке в квадрате).

Если вместо списка передано что-то другое, то возвращать строку 'First arg must be list!'.

Если разделитель не является строкой, то возвращать строку 'Second arg must be str!'.

Если список пуст, то возвращать пустой tuple().

ВНИМАНИЕ: в списке могут быть элементы любого типа (нужно конвертировать в строку).
"""

def list_to_str(a: list, b: str):
    if type(a) is not list:
        return 'First arg must be list!'
    elif type(b) is not str:
        return 'Second arg must be str!'
    elif len(a) == 0:
        return tuple()
    new_1 = b.join([str(elem) for elem in a])
    return new_1, new_1.count(b) ** 2

    # # list -> string
    # # только, если все элементы list являются str
    # list_count = ['one', 'two', 'many']
    # s = ', '.join(list_count)
    # print(s)  # one, two, many
    #
    # # list -> string
    # list_misc = ["a", "sample", 44, 55, "program"]
    # s = '_'.join([str(elem) for elem in list_misc])
    # print(s)  # a_sample_44_55_program
    #
    # l_str = str(list_misc)
    # print(l_str)
    #
    # return list(zip_longest(cars, years, fillvalue='???'))