"""
Функция dict_to_list.

Принимает 1 аргумент: словарь.

Возвращает список: [
список ключей,
список значений,
количество уникальных элементов в списке ключей,
количество уникальных элементов в списке значений
].

Если вместо словаря передано что-то другое, то возвращать строку 'Must be dict!'.

Если dict пуст, то возвращать ([], [], 0, 0).
"""


def dict_to_list(my_dict: dict):
    if type(my_dict) is not dict:
        return 'Must be dict!'
    elif len(my_dict) == 0:
        return ([], [], 0, 0)
    dict_keys = list(my_dict.keys())
    dict_val = list(my_dict.values())
    unique_keys = set(dict_keys)
    unique_val = set(dict_val)

    return dict_keys, dict_val, len(unique_keys), len(unique_val)


if __name__ == '__main__':
    ...

    # key_equal_val = False
    # for a in my_dict.keys():
    #     for b in my_dict.values():
    #         if a == b:
    #             key_equal_val = True
    #             break
    #     if key_equal_val:
    #         break
    # val_equal_key = False
    # for c in my_dict.values():
    #     for d in my_dict.keys():
    #         if c == d:
    #             val_equal_key = True
    #             break
    #     if val_equal_key:
    #         break
