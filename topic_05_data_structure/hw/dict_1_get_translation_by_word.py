"""
Функция get_translation_by_word.

Принимает 2 аргумента:
ru-eng словарь содержащий {ru_word: [eng_1, eng_2 ...], ...}
слово для поиска в словаре (ru).

Возвращает все варианты переводов (list), если такое слово есть в словаре,
если нет, то "Can't find Russian word: {word}".

Если вместо словаря передано что-то другое, то возвращать строку "Dictionary must be dict!".
Если вместо строки для поиска передано что-то другое, то возвращать строку "Word must be str!".

Если словарь пустой, то возвращать строку "Dictionary is empty!".
Если строка для поиска пустая, то возвращать строку "Word is empty!".
"""


def get_translation_by_word(ru_eng_dict: dict, word: str):
    if type(ru_eng_dict) != dict:
        return "Dictionary must be dict!"
    elif type(word) is not str:
        return "Word must be str!"
    elif len(ru_eng_dict) == 0:
        return "Dictionary is empty!"
    elif len(word) == 0:
        return 'Word is empty!'

    if word in ru_eng_dict:
        return list(ru_eng_dict.get(word))
    else:
        return f"Can't find Russian word: {word}"


if __name__ == '__main__':
    print(get_translation_by_word({'joy': 'радость', 'sadness': 'грусть', 'love': 'любовь'}, "love"))








# a = [1, 2, 3, 4, 5]
#
# dicti = {'joy': 'радость', 'sadness': 'грусть', 'love': 'любовь'}
#
# if 'joy' in dicti:
