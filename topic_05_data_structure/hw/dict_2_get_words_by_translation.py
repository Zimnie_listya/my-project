"""
Функция get_words_by_translation.

Принимает 2 аргумента:
ru-eng словарь содержащий {ru_word: [eng_1, eng_2 ...], ...}
слово для поиска в словаре (eng).

Возвращает список всех вариантов переводов (ru), если такое слово есть в словаре (eng),
если нет, то "Can't find English word: {word}".

Если вместо словаря передано что-то другое, то возвращать строку "Dictionary must be dict!".
Если вместо строки для поиска передано что-то другое, то возвращать строку "Word must be str!".

Если словарь пустой, то возвращать строку "Dictionary is empty!".
Если строка для поиска пустая, то возвращать строку "Word is empty!".
"""


def get_words_by_translation(ru_eng_dict: dict, word: str):
    if type(ru_eng_dict) != dict:
        return "Dictionary must be dict!"
    elif type(word) != str:
        return "Word must be str!"
    elif len(ru_eng_dict) == 0:
        return "Dictionary is empty!"
    elif len(word) == 0:
        return "Word is empty!"
    list_rus_words = list()
    for rus, eng in ru_eng_dict.items():
        if word in eng:
            list_rus_words.append(rus) # верните все слова в словарь присоеденив одно к другому
    return list_rus_words if len(list_rus_words) else f"Can't find English word: {word}"


# if __name__ == '__main__':
