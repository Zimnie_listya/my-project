"""
Функция pow5_start_inf_step.

Принимает 2 аргумента: число start, step.

Возвращает генератор-выражение состоящий из
значений в 5 степени от start до бесконечности с шагом step.

Пример: start=3, step=2 результат 3^5, 5^5, 7^5, 9^5 ... (infinity).

Если start или step не являются int, то вернуть строку 'Start and Step must be int!'.
"""
import itertools

def pow5_start_inf_step(start: int, step: int):
    if type(start) is not int or type(step) is not int:
        return 'Start and Step must be int!'
    return (x ** 5 for x in itertools.count(start, step))

# TODO зачем тут итертулс а в другом генераторе не надо