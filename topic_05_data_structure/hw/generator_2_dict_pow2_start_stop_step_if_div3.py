"""
Функция dict_pow2_start_stop_step_if_div3.

Принимает 3 аргумента: числа start, stop, step.

Возвращает генератор-выражение состоящий из кортежа (аналог dict):
(значение, значение в квадрате)
при этом значения перебираются от start до stop (не включая) с шагом step
только для чисел, которые делятся на 3 без остатка.

Пример: start=0, stop=10, step=1 результат ((0, 0), (3, 9), (6, 36), (9, 81)).

Если start или stop или step не являются int, то вернуть строку 'Start and Stop and Step must be int!'.
Если step равен 0, то вернуть строку "Step can't be zero!"
"""

def dict_pow2_start_stop_step_if_div3(start: int, stop: int, step: int):
    if type(start) is not int or type(stop) is not int or type(step) is not int:
        return 'Start and Stop and Step must be int!'
    elif step == 0:
        return "Step can't be zero!"
    return ((a, a ** 2) for a in range(start, stop, step) if a % 3 == 0)
