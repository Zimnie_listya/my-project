"""
Функция magic_mul.

Принимает 1 аргумент: список my_list.

Возвращает список, который состоит из
[первого элемента my_list]
+ [три раза повторенных списков my_list]
+ [последнего элемента my_list].

Пример:
входной список [1,  'aa', 99]
результат [1, 1,  'aa', 99, 1,  'aa', 99, 1,  'aa', 99, 99].

Если вместо списка передано что-то другое, то возвращать строку 'Must be list!'.
Если список пуст, то возвращать строку 'Empty list!'.
"""


def magic_mul(my_list):
    if type(my_list) is not list:
        return 'Must be list!'
    elif len(my_list) == 0:
        return 'Empty list!'

    return [my_list[0], *(my_list * 3), my_list[-1]] #*(my_ist *3) - раскрывает список и выбрасывает переменные


    # result_list = []
    # my_list_1 = my_list[0]
    # my_list_2 = my_list*3
    # my_list_3 = my_list[-1]
    #
    # result_list.append(my_list_1)
    # result_list.extend(my_list_2)
    # result_list.append(my_list_3)
    #
    # return result_list


if __name__ == '__main__':
    magic_mul([1, 2, 88])

    # my_list1 = magic_mul(my_list)
    # mylist_1 = (my_list[1] + my_list*3 + my_list[-1])
