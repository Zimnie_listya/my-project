"""
Функция pow_start_stop.

Принимает числа start, stop.

Возвращает список состоящий из квадратов значений от start до stop (не включая).

Пример: start=3, stop=6, результат [9, 16, 25].

Если start или stop не являются int, то вернуть строку 'Start and Stop must be int!'.
"""

def pow_start_stop(start: int, stop: int):
    if type(start) is not int or type(stop) is not int:
        return "Start and Stop must be int!"
    return list(n ** 2 for n in range(start, stop))
