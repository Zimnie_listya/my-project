"""
Функция get_info_for_3_set.

Принимает 3 аргумента: множества my_set_left, my_set_mid и my_set_right.

Возвращает dict с информацией:
{
'left == mid == right': True/False,
'left == mid': True/False,
'left == right': True/False,
'mid == right': True/False,

'left & mid': set(...), # intersection
'left & right': set(...),   # intersection
'mid & right': set(...),# intersection

'left <= mid': True/False, # issubset
'mid <= left': True/False, # issubset
'left <= right': True/False,   # issubset
'right <= left': True/False,   # issubset
'mid <= right': True/False,# issubset
'right <= mid': True/False # issubset
}

Если вместо множеств передано что-то другое, то возвращать строку 'Must be set!'.
"""


def get_info_for_3_set(a: set, b: set, c: set):
    if type(a) != set or type(b) != set or type(c) != set:
        return 'Must be set!'
    return {
            'left == mid == right': a == b == c,
            'left == mid': a == b,
            'left == right': a == c,
            'mid == right': b == c,

            'left & mid': a.intersection(b),
            'left & right': a.intersection(c),
            'mid & right': b.intersection(c),

            'left <= mid': a.issubset(b),
            'mid <= left': b.issubset(a),
            'left <= right': a.issubset(c),
            'right <= left': c.issubset(a),
            'mid <= right': c.issubset(b),
            'right <= mid': b.issubset(c)
            }


if __name__ == '__main__':
    print(get_info_for_3_set({1}, {2}, {3}))

#  return {
#         'left == mid == right': s1 == s2 == s3,
#         'left == mid': s1 == s2,
#         'left == right': s2 == s3,
#         'mid == right': s1 == s3,
#
#         'left & mid': s1.intersection(s2),  # intersection
#         'left & right': s1.intersection(s3),  # intersection
#         'mid & right': s2.intersection(s3),  # intersection
#
#         'left <= mid': s1.issubset(s2),  # issubset
#         'mid <= left': s1.issuperset(s2),  # issubset
#         'left <= right': s1.issubset(s3),  # issubset
#         'right <= left': s1.issuperset(s3),  # issubset
#         'mid <= right': s2.issubset(s3),  # issubset
#         'right <= mid': s2.issuperset(s3)  # issubset
#     }
#
#
# if __name__ == '__main__':
#     print(get_info_for_3_set({1}, {2}, {3}))
