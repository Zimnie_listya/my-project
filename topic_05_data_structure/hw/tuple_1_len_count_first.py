"""
Функция len_count_first.

Принимает 2 аргумента: кортеж my_tuple и строку word.

Возвращает кортеж состоящий из
длины кортежа,
количества word в кортеже my_tuple,
первого элемента кортежа.

Пример:
my_tuple=('55', 'aa', '66')
word = '66'
результат (3, 1, '55').

Если вместо tuple передано что-то другое, то возвращать строку 'First arg must be tuple!'.
Если вместо строки передано что-то другое, то возвращать строку 'Second arg must be str!'.
Если tuple пуст, то возвращать строку 'Empty tuple!'.
"""
from itertools import count


def len_count_first(my_tuple: tuple, word: str):
    if type(my_tuple) is not tuple:
        return 'First arg must be tuple!'
    elif type(word) is not str:
        return 'Second arg must be str!'
    elif len(my_tuple) == 0:
        return 'Empty tuple!'

    return len(my_tuple), my_tuple.count(word), my_tuple[0]

    # A = len(my_tuple)
    # B = my_tuple.count(word)
    # C = my_tuple[0]
    # return A, B, C


if __name__ == '__main__':
    print(len_count_first(('Olga', 1990, 'Boris', 1989, 'Ivan', 1995, 'Lera', 2000), 'Ivan'))