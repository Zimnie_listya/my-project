"""
Функция zip_names.

Принимает 2 аргумента: список с именами и множество с фамилиями.

Возвращает список с парами значений из каждого аргумента.

Если вместо list передано что-то другое, то возвращать строку 'First arg must be list!'.
Если вместо set передано что-то другое, то возвращать строку 'Second arg must be set!'.

Если list пуст, то возвращать строку 'Empty list!'.
Если set пуст, то возвращать строку 'Empty set!'.

Если list и set различного размера, обрезаем до минимального (стандартный zip). # TODO Это вшито в функцию зип?
"""


def zip_names(names: list, surnames: set):
    if type(names) != list:
        return 'First arg must be list!'
    elif type(surnames) != set:
        return 'Second arg must be set!'
    elif len(names) == 0:
        return 'Empty list!'
    elif len(surnames) == 0:
        return 'Empty set!'
    return list(zip(names, surnames))
