"""
Функция save_dict_to_file_classic.

Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

Сохраняет список в файл. Проверить, что записалось в файл.
"""

def save_dict_to_file_classic(path_flower_types, flower_dict):
    with open(path_flower_types, 'w') as flower_types_file:
        flower_types_file.write(str(flower_dict))

if __name__ == '__main__':
    path_flower_types = 'flower_dict.txt'
    save_dict_to_file_classic(path_flower_types, {"стрелец": "тюльпан", "рак": "роза", "водолей": "ромашка"})

    with open(path_flower_types, 'r') as f:
        flower_dict_str = f.read()
        print(flower_dict_str)