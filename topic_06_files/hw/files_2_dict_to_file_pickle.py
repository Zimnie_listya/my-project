"""
Функция save_dict_to_file_pickle.

Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

Сохраняет список в файл. Загрузить словарь и проверить его на корректность.
"""

import pickle
def save_dict_to_file_pickle(your_character, characters_dict):
    with open(your_character, 'wb') as characters_pickle_file:
        pickle.dump(characters_dict, characters_pickle_file)


if __name__ == '__main__':
        characters_dict_exmpl = {1: "энтузиаст", 2: "прагматик", 3: "мечтатель", 4: "трудоголик"}
        path = 'your_character.pkl'

        save_dict_to_file_pickle(path, characters_dict_exmpl)

        with open(path, 'rb') as file_characters:
            dict_loaded = pickle.load(file_characters)

        # проверка
        print(f'type: {type(dict_loaded)}')
        print(f'equal: {characters_dict_exmpl == dict_loaded}')
