"""
Функция read_str_from_file.

Принимает 1 аргумент: строка (название файла или полный путь к файлу).

Выводит содержимое файла (в консоль). (* Файл должен быть предварительно создан и наполнен каким-то текстом.)
"""


def read_str_from_file(segments):
    with open(segments, 'r') as my_file_segments:
        for line in my_file_segments:
            print(line.strip())

if __name__ == '__main__':
    read_str_from_file("segments.txt")