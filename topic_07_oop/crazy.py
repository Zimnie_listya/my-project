class Worker:
    salary = None

    def __init__(self, name, salary, position):
        """

        :type salary: object
        """
        self.name = name
        self.salary = salary
        self.position = position


class Pupil:
    def __init__(self, name, age, marks):
        self.name = name
        self.age = age
        self.marks = marks


worker_3 = Worker('Hanz', 456987678.90, "Loh")
worker_1 = Worker('Миша', 86324.50, "Учитель математики")
worker_2 = Worker('Лена', 3453245.50, "Повар")
pupil_1 = Pupil('Леха', 4, [4, 2, 4])
people = [worker_3, pupil_1, worker_1, worker_2]


def get_min_worker_salary(people):
    min_salary = None
    for item in people:
        if type(item) is Worker:  # проверили на принадлежность классу Worker
            # if min_salary is None:
            #     min_salary = item.salary
            # else:
            #     min_salary = min(min_salary, item.salary)

            min_salary = item.salary if min_salary is None else min(min_salary, item.salary)

            #     min_salary = item.salary
            # else:
            #     if item.salary < min_salary:
            #         min_salary = item.salary

            # min_salary = min(min_salary, person.salary)
    return min_salary


print(get_min_worker_salary(people))

# def get_min_worker_salary(self):
#     min_salary = 10000000000
#     for person in self.people:
#         if type(person) is Worker:
#             min_salary = min(min_salary, person.salary)
#     return min_salary
