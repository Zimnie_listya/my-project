"""
Класс Movie.

Поля:
duration_min,
name,
year.

При создании экземпляра инициализировать поля класса.

Перегрузить оператор __str__, который возвращает строку вида
"Наименование фильма: name | Год выпуска: year | Длительность (мин): duration_min".

Перегрузить оператор __ge__, который
возвращает True, если self.duration_min => other_duration_min, иначе False.
"""


class Movie:
    def __init__(self, duration_min, name, year):
        self.duration_min = duration_min
        self.name = name
        self.year = year

    def __str__(self):
        return f"Наименование фильма: {self.name} | Год выпуска: {self.year} | Длительность (мин): {self.duration_min}"

    def __ge__(self, other):
        return self.duration_min >= other.duration_min
