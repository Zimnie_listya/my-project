"""
Класс Farm.

Поля:
животные (list из произвольного количества Goat и Chicken): zoo_animals
(вначале список пуст, потом добавялем животных методами append и extend самостоятельно),
наименование фермы: name,
имя владельца фермы: owner.

Методы:
get_goat_count: вернуть количество коз на ферме (*Подсказка isinstance или type == Goat),
get_chicken_count: вернуть количество куриц на ферме,
get_animals_count: вернуть количество животных на ферме,
get_milk_count: вернуть сколько молока можно получить в день,
get_eggs_count: вернуть сколько яиц можно получить в день.
"""
#
from topic_07_oop.hw.class_3_1_chicken import Chicken
from topic_07_oop.hw.class_3_2_goat import Goat


class Farm:
    def __init__(self, name, owner):
        self.zoo_animals = list()
        self.name = name
        self.owner = owner

    def get_goat_count(self):
        a = 0
        for i in self.zoo_animals:
            if type(i) == Goat:
                a += 1
        return a

    def get_milk_count(self):
        list_goat = list()
        for a in self.zoo_animals:
            if type(a) == Goat:
                list_goat.append(a)
        d = 0
        for goatushka in list_goat:
            d += goatushka.milk_per_day
        return d

    def get_chicken_count(self):
        return len([i for i in self.zoo_animals if isinstance(i, Chicken)])

    def get_animals_count(self):
        return len(self.zoo_animals)

    def get_eggs_count(self):
        return sum([chik.eggs_per_day for chik in self.zoo_animals if isinstance(chik, Chicken)])

#
if __name__ == '__main__':
    new_farm = Farm('Sweet home', 'Olga')
    new_farm.zoo_animals.extend([
        Chicken('Lola', 1, 2),
        Chicken('Poop', 3, 3),
        Chicken('Rumba', 1, 1),
        Goat('Kek', 4, 5),
        Goat('Loop', 11, 1),
    ])
    print(new_farm.get_chicken_count())
    print(new_farm.get_goat_count())
    print(new_farm.get_animals_count())
    print(new_farm.get_eggs_count())
    print(new_farm.get_milk_count())
