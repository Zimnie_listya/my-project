"""
Класс Pupil.

Поля:
имя: name,
возраст: age,
dict с оценками: marks (пример: {'math': [3, 5], 'english': [5, 5, 4] ...].

Методы:
get_all_marks: получить список всех оценок,
get_avg_mark_by_subject: получить средний балл по предмету (если предмета не существует, то вернуть 0),
get_avg_mark: получить средний балл (все предметы),
__le__: вернуть результат сравнения (<=) средних баллов (все предметы) двух учеников.
"""

class Pupil:
    def __init__(self, name, age, marks):
        self.name = name
        self.age = age
        self.marks = marks

    def get_all_marks(self):
        val = self.marks.values()
        new_list = []
        for i in val:
            new_list.extend(i)
        return new_list

    def get_avg_mark_by_subject(self, subject):
        if subject in self.marks.keys():
            subj_marks = self.marks.get(subject, ())
            return sum(subj_marks) / len(subj_marks) if subj_marks else subj_marks
        return 0

    def get_avg_mark(self):
        all_marks = self.get_all_marks()
        return sum(all_marks) / len(all_marks) if all_marks else 0

    def __le__(self, other):
        return self.get_avg_mark() <= other.get_avg_mark()


if __name__ == '__main__':
    pupil_vasya = Pupil('Olga', 14, {'math': [], 'english': []})
    print(pupil_vasya.get_avg_mark())

