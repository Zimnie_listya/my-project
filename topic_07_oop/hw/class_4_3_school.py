"""
Класс School.

Поля:
список людей в школе (общий list для Pupil и Worker): people,
номер школы: number.

Методы:
get_avg_mark: вернуть средний балл всех учеников школы
get_avg_salary: вернуть среднюю зп работников школы
get_worker_count: вернуть сколько всего работников в школе
get_pupil_count: вернуть сколько всего учеников в школе
get_pupil_names: вернуть все имена учеников (с повторами, если есть)
get_unique_worker_positions: вернуть список всех должностей работников (без повторов, подсказка: set)
get_max_pupil_age: вернуть возраст самого старшего ученика
get_min_worker_salary: вернуть самую маленькую зп работника
get_min_salary_worker_names: вернуть список имен работников с самой маленькой зп
(список из одного или нескольких элементов)
"""

from topic_07_oop.hw.class_4_2_worker import Worker
from topic_07_oop.hw.class_4_1_pupil import Pupil
from statistics import mean


# starik_vozrast = 0
# for pridurka in school:
#     starik = max(starik_vozrast, pridurka.age)
#
# sum([30, 30, 5]) / len([30, 30, 5])

class School:
    def __init__(self, people, number):
        self.people = people
        self.number = number

    def get_avg_mark(self):
        val = [t.get_avg_mark() for t in self.get_pupil()]
        av_marks_list = []
        for x in val:
            av_marks_list.append(x)
        return mean(av_marks_list)

    def get_pupil(self):
        return [p for p in self.people if isinstance(p, Pupil)]

    def get_worker(self):
        return [w for w in self.people if isinstance(w, Worker)]

    def get_avg_salary(self):
        return mean([money.salary for money in self.people if isinstance(money, Worker)])

    def get_worker_count(self):
        return len(self.get_worker())

    def get_pupil_count(self):
        return len(self.get_pupil())

    def get_pupil_names(self):
        return [np.name for np in self.get_pupil()]

    def get_unique_worker_positions(self):
        return set([wp.position for wp in self.get_worker()])

    def get_max_pupil_age(self):
        elder_age = None
        for item in self.people:
            if type(item) is Pupil:
                if elder_age is None:
                    elder_age = item.age
                else:
                    elder_age = max(elder_age, item.age)
        return elder_age


    def get_min_worker_salary(self):
        min_salary = None
        for item in self.people:
            if type(item) is Worker:
                min_salary = item.salary if min_salary is None else min(min_salary, item.salary)
        return min_salary

    def get_min_salary_worker_names(self):
        names = self.get_min_worker_salary()
        return [i.name for i in self.get_worker() if i.salary == names]


if __name__ == '__main__':
    pupil_1 = Pupil('Петя',
                    15,
                    {
                        'math': [3, 3, 3],
                        'history': [4, 4, 4],
                        'english': [5, 5, 5]
                    })
    pupil_2 = Pupil('Маша',
                    15,
                    {
                        'math': [4, 4, 4],
                        'history': [4, 4, 4],
                        'english': [4, 4, 4]
                    })

    pupil_3 = Pupil('Маша',
                    13,
                    {
                        'math': [5, 5, 5],
                        'history': [5, 5, 5],
                        'english': [5, 5, 5]
                    })

    worker_1 = Worker('Миша',
                      86324.50,
                      "Учитель математики")

    worker_2 = Worker('Лена',
                      56324.50,
                      "Повар")

    worker_3 = Worker('Василиса',
                      56324.50,
                      "Учитель математики")
    school = School([pupil_1, pupil_2, pupil_3, worker_1, worker_2, worker_3], 333)

    print(school.get_avg_mark())
    print(school.get_avg_salary())
    print(school.get_max_pupil_age())
